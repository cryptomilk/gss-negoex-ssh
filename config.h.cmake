/* Name of package */
#cmakedefine NEGOEX_PACKAGE "${NEGOEX_PACKAGE}"

/* Version number of package */
#cmakedefine NEGOEX_VERSION "${NEGOEX_VERSION}"

/**************************** OPTIONS ****************************/
#cmakedefine HAVE_GCC_THREAD_LOCAL_STORAGE 1
#cmakedefine HAVE_CONSTRUCTOR_ATTRIBUTE 1
#cmakedefine HAVE_DESTRUCTOR_ATTRIBUTE 1
#cmakedefine HAVE_PRAGMA_INIT 1
#cmakedefine HAVE_PRAGMA_FINI 1
#cmakedefine HAVE_FALLTHROUGH_ATTRIBUTE 1

/*************************** ENDIAN *****************************/
/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#cmakedefine WORDS_BIGENDIAN 1
